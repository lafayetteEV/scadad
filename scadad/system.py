#import scadad.can_bus
import scadad.subsystems as subsystems
import scadad.networks as networks

class System(object):

    def __init__(self, topology):
        self.networks = {}
        self.subsystems = {}

        # Physical Systems
        for k,v in topology['physical'].items():
            # If it is a dictionary then it is a network
            if type(v) == type({}):
                self.add_network(k, v)
            # Otherwise it is a subsystem
            else:
                self.add_subsystem(k, v)

        # Virtual Systems
        for k, v in topology['virtual'].items():
            if v:
                self.add_subsystem(v)
            else:
                pass
                #self.add_subsystem()
                #self.add_subsystem('virtual', vs)

    def add_network(self, name, topology):
        if name[:3] == 'can' or name[:4] == 'vcan':
            network = networks.CANBus(name)
        else:
            return
        for k, v in topology.items():
            self.add_subsystem((network, k), v)
        self.networks[repr(network)] = network

    def add_subsystem(self, location, systype):
        subsystem = subsystems.TYPE_MAP[systype](location)
        self.subsystems[repr(subsystem)] = subsystem

    def toDict(self, graphs=False):
        subsystems = []
        for ss in self.subsystems:
            subsystems.append(ss)
        d = {
            'type': 'system'
        }
        return d

    def __str__(self):
        return '<System>\n{} subsystems loaded.\n{}'.format(len(self.subsystems.keys()), list(self.subsystems.values()))
