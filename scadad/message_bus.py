# Copyright 2016 Brendon Carroll
'''A singleton message bus.  It's just a dispatcher from scadad.util.
'''
from .util.dispatcher import Dispatcher

d = Dispatcher()

def getbus():
    return d
