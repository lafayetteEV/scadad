import asyncio

import yaml

from .system import System
from .web_api import WebAPI

ST_FILENAME = 'system_topology.yml'

def main():
    # Setup web interface
    with open(ST_FILENAME) as f:
        top = yaml.load(f)
    s = System(top)
    print(s)

    wa = WebAPI(s)

    loop = asyncio.get_event_loop()
    loop.run_forever()

if __name__ == '__main__':
    main()
