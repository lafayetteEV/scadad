import asyncio

class Channel(object):
    """Used to communicate through read calls with a dispatcher.
    Very similar idea to Golangs channels, or Erlang mailboxes.
    """

    def __init__(self, d):
        self.d = d
        self.queue = d.get_queue(id(self))

    async def __anext__(self):
        return await self.read()

    async def read(self):
        return await self.queue.get()

    def __del__(self):
        self.d.remove_queue(id(self))

class Dispatcher(object):
    """A dispatcher class for use with the asyncio library. A dispatcher
    can be used to broadcast events to multiple listeners.
    Dispatchers are async iterators.
    To listen for events:
        d = Dispatcher()
        async for event in d:
            print(event)

    To dispatch an event:
        d.dispatch('event')
        # or
        d.dispatch(['myevent', 4, 5])
    """
    def __init__(self):
        self.queues = {}

    def get_queue(self, idn):
        self.queues[idn] = asyncio.Queue(1)
        return self.queues[idn]

    def remove_queue(self, idn):
        del self.queues[idn]

    async def __aiter__(self):
        return Channel(self)

    async def dispatch(self, event):
        # Need list here to avoid modifying the dict during iteration
        for q in list(self.queues.values()):
            await q.put(event)


if __name__ == '__main__':
    loop = loop = asyncio.get_event_loop()
    d = Dispatcher()

    async def c1():
        for i in range(10):
            await asyncio.sleep(.5)
            await d.dispatch('testsdfsgdfg')
            print(d.queues)
        print(d.queues)

    async def c2():
        asyncio.sleep(1)
        x = 0
        async for event in d:
            print(event)
            x += 1
            if x > 5:
                break

    tasks = [
        asyncio.ensure_future(c1()),
        asyncio.ensure_future(c2())
    ]
    loop.run_until_complete(asyncio.wait(tasks))
    loop.close()
