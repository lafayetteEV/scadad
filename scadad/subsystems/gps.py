from .subsystem import Subsystem

class GPS(Subsystem):
    systype = "gps"
    next_id = 0

    def __init__(self, location):
        super().__init__(location)
