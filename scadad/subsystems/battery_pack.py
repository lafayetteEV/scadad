# Copyright 2016 Brendon Carroll

import asyncio
import time
import scadad.message_bus
mb = scadad.message_bus.getbus()

from .subsystem import Subsystem
from scadad.measurand import Measurand

class Cell(object):
    '''Represents a single cell in a battery pack.
    '''
    def __init__(self, num):
        self.num = num
        self.temp = Measurand('Celcius')
        self.voltage = Measurand('Volts')

    def update(self, field, value):
        if field==0 or field=='voltage':
            self.temp.update(value)
        elif field==1 or field=='temp':
            return self.temp.update(value)
        print('updated cell', self.num)

    def least_recent(self):
        return min([self.temp, self.voltage], lambda x: x.timestamp)

    def toDict(self, graphs=False):
        return {
            'num': self.num,
            'temp': self.temp,
            'voltage': self.temp,
        }

POLL_RATE = 0.25
CELLS_PER_PACK = 7
STATUS_LENGTH = 4
ADDRESS_SPACE_LENGTH = STATUS_LENGTH + 2 * CELLS_PER_PACK

class BatteryPack(Subsystem):
    '''A battery pack model.  Has information about individual cell voltage
    and temperature, pack state of charge, and pack status.
    '''
    systype = 'battery_pack'
    next_id = 0
    TIMEOUT = POLL_RATE / 2

    def __init__(self, location):
        super().__init__(location)
        self.network, self.address = location
        self.address = self.address.split('-')
        self.raddr = int(self.address[0], 16)

        # Setup model
        self.cells = []
        for i in range(CELLS_PER_PACK):
            self.cells.append(Cell(i))

        # Connect to event loop
        loop = asyncio.get_event_loop()
        loop.create_task(self.can_read())
        loop.create_task(self.can_request())

    def normalize_addr(self, msg):
        addr = msg.addr - self.raddr
        if (addr >= 0 and addr < ADDRESS_SPACE_LENGTH):
            return addr
        else:
            return False

    def update(self, naddr, data):
        if naddr < STATUS_LENGTH:
            print(self.__repr__(), 'status update')
            changed = False
        else:
            cell = self.cells[(naddr - STATUS_LENGTH+1) // 2]
            changed = cell.update(naddr % 2, data)
        return changed

    async def can_read(self):
        """Coroutine to read from the CAN bus, check if the message is relevant
        to this object and if so, handle it appropriately.
        """
        async for msg in self.network:
            naddr = self.normalize_addr(msg)
            if naddr is not False:
                if self.update(naddr, msg.data) or True:
                    await mb.dispatch(self.toDict())

    async def can_request(self):
        while True:
            for addr in range(self.raddr, ADDRESS_SPACE_LENGTH + self.raddr):
                await self.network.request(addr)
                await asyncio.sleep(POLL_RATE / ADDRESS_SPACE_LENGTH)

    def toDict(self, graphs=False):
        cells = []
        for cell in self.cells:
            cells.append(cell.toDict(graphs=graphs))
        d = super().toDict()
        d['cells'] = cells
        return d
