from .subsystem import Subsystem

class PowerSupply(Subsystem):
    systype = "power_supply"
    next_id = 0

    def __init__(self, location):
        super().__init__(location)
