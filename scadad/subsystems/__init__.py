from .motor_controller import MotorController as MotorController
from .battery_pack import BatteryPack as BatteryPack
from .power_supply import PowerSupply as PowerSupply
from .gps import GPS as GPS

TYPE_MAP = {
    'battery_pack': BatteryPack,
    'motor_controller': MotorController,
    'power_supply': PowerSupply,
    'gps': GPS,
    #'jgb': subsystems.JGB,
}
