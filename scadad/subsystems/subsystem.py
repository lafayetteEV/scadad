import abc
import time

class Subsystem(abc.ABC):

    @classmethod
    def get_id_num(self):
        id_num = self.next_id
        self.next_id += 1
        return id_num

    def __init__(self, location):
        self.id_num = self.get_id_num()
        self.location = location
        self.online = False
        self.timestamp = time.time()

    def __repr__(self):
        return self.systype + '-' + str(self.id_num)

    def __str__(self):
        return '{} @ {}'.format(self.__repr__(), self.location)

    def toDict(self):
        d = {
            'type': self.systype,
            'location': self.location,
            'online': self.online,
        }
        return d
