from .subsystem import Subsystem

class MotorController(Subsystem):
    systype = 'motor_controller'
    next_id = 0

    def __init__(self, location):
        super().__init__(location)
