# Copyright 2016 Brendon Carroll
"""Loads the config file when it is first imported and makes it available as
a dictionary to anyone else who imports it.
    import config
    n = config['name']
    path = config['webapi_path']
"""
import yaml

with open('config.yml', 'r') as f:
    config = yaml.load(f)

def __getitem__(self, key):
    return config[key]
