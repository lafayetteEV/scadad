import collections
import time

CACHE_SIZE = 10000

class Measurand(object):

    def __init__(self, unit, calibration_func=None):
        """A value to be measured. uncalibrated and calibrated data is stored
        as tuples in a circular buffer.
        """
        self.unit = unit
        self.calibration_func = calibration_func
        self.history = collections.deque()
        self.current_value = None
        self.timestamp = None

    def update(self, new_value, timestamp=time.time()):
        # new_value is a tuple (uncal, cal)
        new_value = (new_value, self.calibrate(new_value))
        # add it to the history
        self.history.append(new_value)
        self.timestamp = timestamp

        if self.current_value is not None:
            changed = new_value[0] != self.current_value[0]
        else:
            changed = True
        self.current_value = new_value
        return changed

    def calibrate(self, value):
        if self.calibration_func is not None:
            return self.calibration_func(value)
        else:
            return value

    def get_value(self):
        return self.current_value

    def get_graph(self, max_samples=100):
        #TODO: We need to resample the history
        return None

    def toDict(self):
        d = {
            'type': 'measurand',
            'uncalibrated': self.current_value[0],
            'calbrated': self.current_value[1],
            'unit': self.unit
        }
        return d

    def __repr__(self):
        if self.current_value is None:
            self.current_value = ('no-data', 'no-data')
        return u'<{uc} -> {c}{unit} @ {timestamp} >'.format(
            uc=self.current_value[0],
            unit=self.unit,
            c=self.current_value[1],
            timestamp = self.timestamp
        )

if __name__ == '__main__':
    m = Measurand(u'\u2103', lambda x: x/1024*100)
    m.update(400)
    print(m)
    m.update(200)
    print(m)
    print(m.toDict())
