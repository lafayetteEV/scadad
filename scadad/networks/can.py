# Copyright 2016 Brendon Carroll

import asyncio
import socket
import struct
import collections

from scadad.util.dispatcher import Dispatcher

can_frame_fmt = "=IB3x8s"
can_frame_size = struct.calcsize(can_frame_fmt)

CANMessage = collections.namedtuple('CANMessage',
['addr', 'is_request', 'data'])

def build_can_frame(can_id, data):
    data = data.ljust(8, b'\x00')
    can_dlc = len(data)
    return struct.pack(can_frame_fmt, can_id, can_dlc, data)

def dissect_can_frame(frame):
    can_id, can_dlc, data = struct.unpack(can_frame_fmt, frame)
    return (can_id, can_dlc, data[:can_dlc])

class CANBus(Dispatcher):

    def __init__(self, interface):
        super().__init__()
        self.active = True

        # Setup socket
        self.socket = socket.socket(socket.AF_CAN,
            socket.SOCK_RAW, socket.CAN_RAW)
        self.socket.bind((interface,))
        self.socket.setblocking(False)

        # Setup event loop
        self.loop = asyncio.get_event_loop()
        self.loop.create_task(self.read_loop())

    async def read_loop(self):
        while self.active:
            frame = await self.loop.sock_recv(self.socket, can_frame_size)
            (can_id, can_dlc, data) = dissect_can_frame(frame)
            addr = can_id & 0xFFFFFFF
            is_request =  (can_id & ~0xFFFFFFF) > 0

            msg = CANMessage(addr, is_request, data)
            await self.dispatch(msg)
            #print('RECV:',data)

    async def send(self, addr, data):
        frame = build_can_frame(addr, data)
        await self.loop.sock_sendall(self.socket, frame)
        #print('SENT:', frame)

    async def request(self, addr):
        can_id = 0x40000000 | addr
        frame = build_can_frame(can_id, bytes())
        await self.loop.sock_sendall(self.socket, frame)

if __name__ == '__main__':
    cb1 = CANBus('vcan0')
    cb2 = CANBus('vcan0')
    loop = asyncio.get_event_loop()

    async def c1():
        for i in range(10):
            await cb1.send_frame(0x01, b'abcdefgh')
            await asyncio.sleep(.5)

    async def c2():
        async for msg in cb2:
            print(msg)

    async def c3():
        await asyncio.sleep(2)
        x = 0
        async for msg in cb2:
            print('c3 got message')
            x += 1
            if x > 2:
                break

    loop.create_task(c1())
    loop.create_task(c2())
    loop.create_task(c3())
    loop.run_forever()
    loop.close()
