# Copyright 2016 Brendon Carroll
from aiohttp import web
import asyncio

import scadad.config
from scadad.message_bus import getbus
mb = getbus()

class WebAPI(object):

    def __init__(self, system):
        self.system = system
        #self.app = web.Application()
        #app.router.add_route('GET', '*', handle)

        loop = asyncio.get_event_loop()
        loop.create_task(self.test())

    async def handle(self, request):
        pass

    async def handlews(self, request):
        #system.toDict(graphs=True)
        print('ws')

    async def test(self):
        async for msg in mb:
            print(msg)

if __name__ == '__main__':
    w = WebAPI()
