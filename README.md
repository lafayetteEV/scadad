# SCADAd
Supervisory Control And Data Acquisition Daemon

## Python3.5 setup
1. Install Python3.5 if its not installed already
```
sudo apt-get install python3.5
```
2. Make 3.5 the default python3
```
sudo rm /usr/bin/python3
sudo -s /usr/bin/python3.5 /usr/bin/python3
```
3. Verify that it was succesful
```
python3 --version
```

## Installing Dependencies
1. Make sure you have pip3 installed.
```
sudo apt-get install python3-pip
```
2. Navigate to the scadad directory
3. Install the dependencies with pip3
```
pip3 install -r requirements.txt
```

## Running the daemon
```
python3 -m scadad
```

## Specifying System Topology
The System gets its topology from a YAML configuration file.
```
# system_topology.yml

# The first set of keys can be interfaces leading to devices, or devices themselves

# For CAN networks the addresses on the network are keys to the subsystem type
"can0":
  "0x03" : "jgb"
  "0x08" : "motor_controller"
  "0x10" : "battery_pack"
  "0x11" : "battery_pack"
  "0x12" : "battery_pack"

# Or just a device.
"/dev/usb0" : "power_supply"

# Something like this is easy to support too.
"eth0" :
  "192.168.0.11" : "proximity_sensor"
  "192.168.0.105" : "webcam" # self racing car here we come.

```
NOTE: We can recycle JG's PowerSupply class with almost no modification.

Each value would map to a python class representing the subsystem.  So when we see "battery_pack" during bootstrapping we say `can0['0x04'] = BatteryPack()`

The system itself should be "indexable" as well.  So I should be able to say `system['batter_pack_0'].checkStatus()`.  We don't care whether that battery pack is on ethernet, CAN, or USB.  We should be able to refer to it with a logical name.

## Subsystems
A subsystem doesn't have to be physical it could be logical as well.  
Each subsystem model can internally use RRDtool to keep a LRU cache of the time series data it collects.  If necessary a model can use CAN to poll for data.

Models will act as event emitters, and when they change any UI listeners will be notified.  The listener can ask for the model to serialize its state into JSON and send it out to the GUI

Subsystems also have callable methods which can be used to manipulate them in an interface agnostic way.

### Physical Subsystem
This is a real thing connected via some interface.  A single battery pack, a motor controller, a JGB.  Useful for logging data and making real life things happen.

### Logical Subsystem
It would be annoying to have to manually configure each battery pack on it's own with a separate user interface.  Why not write a user interface for a "Battery Manager" subsystem, which references all the subsystems with type "batter_pack"  The system class could have a `getAllOfType('batter_pack')` method.

NOTE: Not sure yet how to specify which logical subsystems to use.  Maybe further divide the configuration file into "physical" and "logical" or "virtual"

## Troubleshooting
 - **I see `OSError`:** Is the can interface up? Is there really a usb device plugged in?

## Web Interface
When the ships main computer is connected to on port 80 it will serve up a javascript application which can be used to monitor the state of the system and control the motor.  The application will immediately upgrade to a websocket connection which will send a stream of JSON objects representing the state of the subsystems. and render them to the DOM.  I recommend using React for this.  It's maintained by facebook (they've been known to do some javascript on the side) and is very intuitive unlike many other front-end frameworks.

#### Why this is maintainable
If someone wants to add a copy of an existing subsystem they literally edit a configuration file and are good to go.

If someone wants to add a new bizarre sensor network to check humidity or seat temperature, they write a class, edit the config file and add a page to the web interface (if the want to control it).  If we do the web interface intelligently read only subsystems could work OOTB.
