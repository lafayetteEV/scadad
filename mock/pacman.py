# Copyright 2016 Brendon Carroll

from scadad.networks import CANBus
import asyncio
import struct

PACMAN = {
    0x0: b'good', # STATUS
    0x1: b'yes', # state of charge
    0x2: 0,# current
    0x3: 100, # total voltage

    # CELL 1
    0x4: 26,
    0x5: 9.5,
    # CELL 2
    0x6: 26,
    0x7: 9.5,
    # CELL 3
    0x8: 26,
    0x9: 9.5,
    # CELL 4
    0xA: 26,
    0xB: 9.5,
    # CELL 5
    0xC: 26,
    0xD: 9.5,
    # CELL 6
    0xC: 26,
    0xD: 9.5,
    # CELL 7
    0xE: 26,
    0xF: 9.5,
}

ROOT_ADDR = 0x30
ADDRSPACE_LENGTH = len(PACMAN.keys())

def get_data(msg):
    addr = msg.addr - ROOT_ADDR
    if addr >= 0 and addr < ADDRSPACE_LENGTH and msg.is_request:
        return PACMAN[addr]

async def main():
    print('Mocking PACMAN on vcan0')
    cb = CANBus('vcan0')
    async for msg in cb:
        data = get_data(msg)
        if data:
            if isinstance(data, int):
                data = data.to_bytes(8, byteorder='big')
            elif isinstance(data, float):
                data = b'\x00'
            await cb.send(msg.addr, data)

if __name__ == '__main__':
    asyncio.ensure_future(main())
    asyncio.get_event_loop().run_forever()
